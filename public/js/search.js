// Leaflet
var markerGroup;
var notif = document.getElementById('search-notif');

function displayMap(lat, lon, zoom) {
    map = L.map('map').setView([lat, lon], zoom);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: '© OpenStreetMap contributors',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoieWthcnRzYWsiLCJhIjoiY2puaGdsc3J4MGNuNzN3dGwxMWxmMmVrdCJ9.0fI9siwAMxB-q4VOrcA0uQ',
        maxZoom: 18,
        minZoom: 2,
    }).addTo(map);

}

// Ajax
var searchField = document.getElementById('search-field');
var submitBtn = document.getElementById('search-btn');
submitBtn.addEventListener("click", getCoords);
function getCoords(e) {
    e.preventDefault();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            var coords = JSON.parse(response);
            if (coords.length === 0) {
                noResults("show");
                clearMarkers();
            } else {
                noResults("hide");
                clearMarkers();
                displayMarkers(coords);
            }
        }
        else if (this.readyState == 4 && this.status != 200) {
            alert(this.status);
        }
    };
    var token = getMeta('csrf-token');
    var search = "search=" + searchField.value;
    xhttp.open("POST", "./search/ajax", true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.setRequestHeader('X-CSRF-TOKEN', token);
    xhttp.send(search);
}

function noResults(task) {
    if (task === 'show') {
        notif.classList.add('visible');
    } else {
        notif.classList.remove('visible');
    }
}

function clearMarkers() {
    if (markerGroup !== undefined) {
        markerGroup.clearLayers();
    }
}

function displayMarkers(coordsArray) {
    markerGroup = L.layerGroup().addTo(map);

    for (var i = 0; i < coordsArray.length; i++) {
        var lat = coordsArray[i].latitude;
        var lon = coordsArray[i].longitude;
        var name = coordsArray[i].name;

        var marker = L.marker([lat, lon]).addTo(markerGroup);
        marker.bindPopup(name);
    }
}

// Others functions
function getMeta(metaName) {
  const metas = document.getElementsByTagName('meta');

  for (let i = 0; i < metas.length; i++) {
    if (metas[i].getAttribute('name') === metaName) {
      return metas[i].getAttribute('content');
    }
  }

  return '';
}

// Code to execute
displayMap(48.8562532, 2.3511688, 12);

notif.onclick = noResults;
