var parts = document.getElementsByClassName('summary');
var subparts = document.getElementsByClassName('article');

function show(element) {
    for (var j = 0; j < subparts.length; j++) {
        parts[j].classList.remove('open');
        subparts[j].classList.remove('visible');
    }
    var subpart = element.nextElementSibling;
    element.classList.add('open');
    subpart.classList.add('visible');
};

for (var i = 0; i < parts.length; i++) {
    parts[i].addEventListener('click', function() {
        show(this);
    });
}
