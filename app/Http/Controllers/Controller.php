<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;

class Controller extends BaseController
{
    public function index(){
        return view('home');

    }
    public function search(){
        return view('search');
    }
    public function map(){
        return view('map');
    }

    public function pas(){
        return view('p-a-s');
    }

    public function call(){
        return view('call');
    }
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}