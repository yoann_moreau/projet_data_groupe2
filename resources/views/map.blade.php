@extends('default')

@section('title')
    Map
@endsection

@section('custom_css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/leaflet-routing-machine.css') }}">
@endsection

@section('content')

<!--button part-->
    <main>
        <article id="def-info">
            <h2 id="def-name"></h2>
            <div id="def-meta-info">
                <p id="def-address"></p>
                <h4>Horaires :</h4>
                <p id="def-schedule"></p>
            </div>
        </article>
        <div id="animation"></div>
        <div id="map" class="map"><button id="run">GO</button></div>
    </main>

@endsection

@section('scripts')
<!--load all scripts-->
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"></script>
    <script src="{{ asset('./js/leaflet-routing-machine.min.js') }}"></script>
    <script src="{{ asset('./js/map.js') }}"></script>
@endsection
