<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Cantin P, Marine G, Quentin M, Yoann M.">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" href="{{ asset('/img/Logo.png') }}" >
    @yield('custom_css')
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" type="text/css">
</head>

<body>
    <header>
        <a href="{{ url('/') }}"><img src="{{ url('/img/Logo.png') }}" alt="Logo PulseFinder" class="icon-header"></a>
        <h1>PulseFinder</h1>
        @if (Request::is('search'))
            <div class="form-container">
                <form method="post" action="">
                    @csrf
                    <input type="text" name="search" id="search-field" placeholder="Rechercher">
                    <input type="submit" name="btn-submit" value="go" id="search-btn">
                </form>
                <div id="search-notif">Aucun résultat</div>
            </div>
        @else
            <a id ="lens" href="{{ url('/search') }}"><img src="{{ url('/img/loupe.png') }}" alt="Icône barre de recherche" class="icon-header"></a>
        @endif
    </header>

    @yield('content')


    <footer>
        <nav class="nav">
            <a href="{{ url('/') }}"><img src="{{ url('/img/home.png') }}" alt="Icône accueil" class="icon-footer"></a>
            <div class="line"></div>
            <a href="{{ url('/pas') }}"><img src="{{ url('/img/picto-soin.png') }}" alt="Icône premiers secours" class="icon-footer"></a>
            <div class="line"></div>
            <a href="{{ url('/call') }}"><img src="{{ url('/img/Emergency.png') }}" alt="Icône numéros d'urgence" class="icon-footer"></a>
        </nav>
    </footer>

    @yield('scripts')

</body>

</html>
