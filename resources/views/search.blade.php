@extends('default')

@section('title')
    Recherche
@endsection

@section('custom_css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/leaflet-routing-machine.css') }}">
@endsection

@section('content')

<!--button part-->
    <main>
        <div id="map" class="map"></div>
        <article id="research" class="infos">
			<p>Veuillez rentrer ci-dessus votre critère de recherche.</p>
        </article>
    </main>

@endsection

@section('scripts')
<!--load all scripts-->
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"></script>
    <script src="{{ url('js/leaflet-routing-machine.min.js') }}"></script>
    <script src="{{ url('js/search.js') }}"></script>
    <script src="{{ url('js/research.js') }}"></script>
@endsection