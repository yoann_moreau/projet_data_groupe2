#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: defibrillators
#------------------------------------------------------------

CREATE TABLE defibrillators(
        id             Int  Auto_increment  NOT NULL ,
        arrondissement Int NOT NULL ,
        type           Varchar (50) NOT NULL ,
        name           Varchar (100) NOT NULL ,
        address        Varchar (250) NOT NULL ,
        code           Int NOT NULL ,
        city           Varchar (50) NOT NULL ,
        def_number     Int NOT NULL ,
        latitude       Float NOT NULL ,
        longitude      Float NOT NULL
	,CONSTRAINT defibrillators_PK PRIMARY KEY (id)
)ENGINE=InnoDB;

