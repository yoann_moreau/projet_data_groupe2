<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route ::get('','Controller@index');
Route ::get('/map','Controller@map');
Route ::get('/pas','Controller@pas');
Route ::get('/call','Controller@call');

Route ::get('/map/ajax','AjaxController@map');
Route ::post('/search/ajax','AjaxController@searchAjax');

Route ::get('/search','Controller@search');
// Route ::get('/search', ['as' => 'search', 'uses' => 'Controller@search']);
